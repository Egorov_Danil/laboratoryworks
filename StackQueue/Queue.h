#pragma once
template <class T>
class Queue
{
private:
	T * Array;
	unsigned int size;
	unsigned int begin;
	unsigned int end;
	unsigned int index;
public:

	Queue(int _size) : size(_size), index(0), end(0), begin(0)
	{
		Array = new T[size];
	}


	Queue(const Queue &otherQueue) : begin(0), end(otherQueue.size), index(otherQueue.index), size(2 * otherQueue.size)
	{
		Array = new T[size];

		if (otherQueue.end > otherQueue.begin)
		{
			int j = 0;
			for (int i = otherQueue.begin; i < otherQueue.end; i++) {
				Array[j] = otherQueue.Array[i];
				j++;
			}

			for (int i = otherQueue.end; i < otherQueue.begin; i++) {
				Array[j] = otherQueue.Array[i];
				j++;
			}
		}
		else
		{
			int j = 0;
			for (int i = otherQueue.begin; i < otherQueue.size; i++) {
				Array[j] = otherQueue.Array[i];
				j++;
			}

			for (int i = 0; i < otherQueue.end; i++) {
				Array[j] = otherQueue.Array[i];
				j++;
			}
		}
		delete[] otherQueue.Array;
	}

	void push(T value)
	{
		if (index < size) {
			Array[end++] = value;
			index++;

			if (end >= size) end = 0;
		}
		else cout << "Queue overflow" << endl;
	}

	T pop()
	{
		if (index > 0) {
			T returnValue = Array[begin++];
			index--;
			if (begin >= size) begin = 0;

			return returnValue;
		}
		else cout << "Queue empty" << endl;
		return 0;
	}

	const T peek()
	{
		if (index > 0) return Array[begin];
		return 0;
	}


	void printQueue()
	{
		cout << "----------------" << endl;

		if (end > begin)
		{
			for (int i = begin; i < end; i++) {
				cout << Array[i] << endl;
			}

			for (int i = end; i < begin; i++) {
				cout << Array[i] << endl;
			}
		}
		else
		{
			for (int i = begin; i < size; i++) {
				cout << Array[i] << endl;
			}

			for (int i = 0; i < end; i++) {
				cout << Array[i] << endl;
			}
		}

	}

};