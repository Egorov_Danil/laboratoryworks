#include "stdafx.h"
#include <iostream>
#include <string>
#include "Stack.h"
#include "Queue.h"
using namespace std;

void QueueTest() {
	Queue<int> A(10);

	cout << "Тестирование струкутры данных очередь:" << endl;
	cout << "Выделили память под 10 эллементов" << endl;
	for (int i = 1; i < 11; i++) {
		A.push(i);
		cout << "Положили значение " << i << endl;
	}

	cout << "Проверка на переполнение:" << endl;
	cout << "Попытка положить значение " << 11 << endl;
	A.push(11);

	for (int i = 1; i < 11; i++) {
		A.pop();
		cout << "Удаляем значение " << i << endl;
	}

	cout << "Проверка на пустоту:" << endl;
	cout << "Попытка удалить значение " << endl;
	A.pop();
}

void StackTest() {
	Stack<int> A(10);

	cout << "Тестирование струкутры данных стек:" << endl;
	cout << "Выделили память под 10 эллементов" << endl;
	for (int i = 1; i < 11; i++) {
		A.push(i);
		cout << "Положили значение " << i << endl;
	}

	cout << "Проверка на переполнение:" << endl;
	cout << "Попытка положить значение " << 11 << endl;
	A.push(11);

	for (int i = 1; i < 11; i++) {
		A.pop();
		cout << "Удаляем значение " << i << endl;
	}

	cout << "Проверка на пустоту:" << endl;
	cout << "Попытка удалить значение " << endl;
	A.pop();
}

int main()
{
	setlocale(LC_ALL, "Russian");
	int a;
	do {
		cout << "1 - Тестирование структуры данных стек" << endl;
		cout << "2 - Тестирование структуры данных очередь" << endl;
		cout << "3 - Выход" << endl;
		
		cin >> a;
		if (a == 1) StackTest();
		else if (a == 2) QueueTest();
		else a = 3;

	} while (a != 3);
	
}