#pragma once
template <class T>
class Stack
{
private:
	T * Array;
	unsigned int size;
	unsigned int index;
public:

	Stack(int _size) : size(_size), index(0)
	{
		Array = new T[size];
	}

	Stack(const Stack &otherStack) : size(2 * otherStack.size), index(otherStack.index)
	{
		Array = new T[size];

		for (int i = 0; i < index; i++) {
			Array[i] = otherStack.Array[i];
		}
			
	}

	~Stack() {
		delete[] Array;
	}

	void push(T value)
	{
		if (index < size) Array[index++] = value;
		else cout << "Stack overflow" << endl;
	}

	T pop()
	{
		if (index > 0) {
			index--;
			return Array[index];
		}
		else cout << "Stack empty" << endl;
		return 0;
	}

	const T peek()
	{
		if (index > 0) return Array[index - 1];
		return 0;
	}


	void printStack()
	{
		cout << "----------------" << endl;
		for (int i = index - 1; i >= 0; i--)
			cout << Array[i] << endl;
	}

};