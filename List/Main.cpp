#include "stdafx.h"
#include <iostream>
#include "Single_Linked_List.h"
#include "Double_Linked_List.h"
using namespace std;

void SingleLinkedListTest() {
	Single_Linked_List<int> A;
	Single_Linked_List<int> B;
	cout << "Тестирование однсвязного списка: " << endl;

	for (int i = 1; i < 6; i++) {
		A.Add(i);
	}
	for (int i = 6; i < 11; i++) {
		B.Add(i);
	}
	cout << "Создали два списка" << endl;
	cout << "Положили 5 элементов в каждый" << endl;
	cout << "Выполнили слияние списков" << endl;
	A.Merge(B);
	A.Show();

	cout << "Сортируем список и выводим" << endl;
	A.Sort();
	A.Show();

	cout << "Проверка на пустоту " << endl;
	A.CheckVoid();
	cout << "Удаление списка" << endl;
	A.DeleteList();
	cout << "Проверка на пустоту " << endl;
	A.CheckVoid();
}

void DoubleLinkedListTest() {
	Double_Linked_List<int> A;
	Double_Linked_List<int> B;
	cout << "Тестирование двусвязного списка: " << endl;

	for (int i = 1; i < 6; i++) {
		A.Add(i);
	}
	for (int i = 6; i < 11; i++) {
		B.Add(i);
	}
	cout << "Создали два списка" << endl;
	cout << "Положили 5 элементов в каждый" << endl;
	cout << "Выполнили слияние списков" << endl;
	A.Merge(B);
	A.Show();

	cout << "Сортируем список и выводим" << endl;
	A.Sort();
	A.Show();

	cout << "Проверка на пустоту " << endl;
	A.CheckVoid();
	cout << "Удаление списка" << endl;
	A.DeleteList();
	cout << "Проверка на пустоту " << endl;
	A.CheckVoid();
}

int main()
{
	setlocale(LC_ALL, "Russian");

	
	int a;

	do {
		cout << "1 - Тестирование однсвязного списка" << endl;
		cout << "2 - Тестирование двусвязного списка" << endl;
		cout << "3 - Выход" << endl;

		cin >> a;
		if (a==1) DoubleLinkedListTest();
		else if (a==2) SingleLinkedListTest();
		else a = 3;

	} while ( a!=3 );
	



}