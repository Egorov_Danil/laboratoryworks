#pragma once

template<typename T>
class Single_Linked_Node
{
public:
	T key;
	Single_Linked_Node *Next;
};


template<typename T>
class Single_Linked_List
{
	Single_Linked_Node<T> *Head;

public:
	Single_Linked_List() { Head = NULL; }

	void Add(T _key)
	{
		Single_Linked_Node<T> *temp = new Single_Linked_Node<T>;
		temp->key = _key;
		temp->Next = Head;
		Head = temp;
	}

	void Delete(T _key)
	{
		Single_Linked_Node<T> *tmp1 = Head;
		Single_Linked_Node<T> *tmp2;
		if (tmp1->key != _key)
		{
			while (tmp1->Next->key != _key) tmp1 = tmp1->Next;
			tmp2 = tmp1->Next;
			tmp1->Next = tmp1->Next->Next;
			delete tmp2;
		}
		else
		{
			Head = tmp1->Next;
			delete tmp1;
		}
	}

	Single_Linked_Node<T> GetFirst() {
		return *Head;
	}

	Single_Linked_Node<T> GetLast() {
		Single_Linked_Node<T> *Last = Head;
		while (Last->Next != NULL) Last = Last->Next;
		cout << Last->key << endl;
		return *Last;
	}

	void CheckVoid() {
		if (Head == NULL) cout << "List is empty" << endl;
		else cout << "List is not empty" << endl;
	}

	int GetCount() {
		int count = 0;
		Single_Linked_Node<T> *tmp = Head;
		while (tmp != NULL) {
			count++;
			tmp = tmp->Next;
		}
		return count;
	}

	void Show()
	{
		Single_Linked_Node<T> *temp = Head;
		cout << "---------------------------" << endl;
		while (temp != NULL)
		{
			cout << temp->key << " ";
			temp = temp->Next;
		}
		cout << endl << "---------------------------" << endl;
	}

	void Swap(T _key1, T _key2)
	{
		Single_Linked_Node<T> *node1 = Head;
		Single_Linked_Node<T> *node2 = Head;

		while (node1->key != _key1) node1 = node1->Next;
		while (node2->key != _key2) node2 = node2->Next;

		T tmp = node1->key;
		node1->key = node2->key;
		node2->key = tmp;
	}

	void DeleteList() {
		Single_Linked_Node<T> *tmp1 = Head;
		Single_Linked_Node<T> *tmp2;

		while (tmp1 != NULL) {
			tmp2 = tmp1;
			tmp1 = tmp1->Next;
			delete tmp2;
		}

		Head = NULL;
	}

	void AddToEnd(T _key) {
		Single_Linked_Node<T> *tmp = Head;
		while (tmp->Next != NULL) tmp = tmp->Next;

		Single_Linked_Node<T> *node = new Single_Linked_Node<T>;
		node->key = _key;
		tmp->Next = node;
		node->Next = NULL;
	}

	void Sort() {

		Single_Linked_Node<T> *node1;
		Single_Linked_Node<T> *node2;

		for (int i = 0; i < GetCount(); i++)
		{
			node1 = Head;
			node2 = node1->Next;

			while (node2 != NULL)
			{
				if (node1->key > node2->key)
				{
					Swap(node1->key, node2->key);
				}
				node1 = node1->Next;
				node2 = node2->Next;
			}
		}

	}

	void Merge(Single_Linked_List & B) {

		Single_Linked_Node<T> *tmp = B.Head;

		while (tmp != NULL)
		{
			AddToEnd(tmp->key);
			tmp = tmp->Next;
		}
	}
};