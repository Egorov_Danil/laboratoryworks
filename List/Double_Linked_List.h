#pragma once

template<typename T>
class Double_Linked_Node
{
public:
	T key;
	Double_Linked_Node *Next;
	Double_Linked_Node *Prev;
};


template<typename T>
class Double_Linked_List
{
	Double_Linked_Node<T> *Head;

public:
	Double_Linked_List() { Head = NULL; }

	void Add(T _key)
	{
		Double_Linked_Node<T> *temp = new Double_Linked_Node<T>;
		temp->key = _key;
		temp->Next = Head;

		if (Head != NULL) temp->Next->Prev = temp;
		temp->Prev = NULL;

		Head = temp;
	}

	void Delete(T _key)
	{
		Double_Linked_Node<T> *tmp1 = Head;
		Double_Linked_Node<T> *tmp2;
		if (tmp1->key != _key)
		{
			while (tmp1->Next->key != _key) tmp1 = tmp1->Next;
			tmp2 = tmp1->Next;
			tmp1->Next = tmp1->Next->Next;
			if (tmp2->Next != NULL) tmp2->Next->Prev = tmp1;
			delete tmp2;
		}
		else
		{
			Head = tmp1->Next;
			Head->Prev = NULL;
			delete tmp1;
		}
	}

	Double_Linked_Node<T> GetFirst() {
		return *Head;
	}

	Double_Linked_Node<T> GetLast() {
		Double_Linked_Node<T> *Last = Head;
		while (Last->Next != NULL) Last = Last->Next;
		cout << Last->key << endl;
		return *Last;
	}

	void CheckVoid() {
		if (Head == NULL) cout << "List is empty" << endl;
		else cout << "List is not empty" << endl;
	}

	int GetCount() {
		int count = 0;
		Double_Linked_Node<T> *tmp = Head;
		while (tmp != NULL) {
			count++;
			tmp = tmp->Next;
		}
		return count;
	}

	void Show()
	{
		Double_Linked_Node<T> *temp = Head;
		cout << "---------------------------" << endl;
		while (temp != NULL)
		{
			cout << temp->key << " ";
			temp = temp->Next;
		}
		cout << endl << "---------------------------" << endl;

	}

	void Swap(T _key1, T _key2)
	{
		Double_Linked_Node<T> *node1 = Head;
		Double_Linked_Node<T> *node2 = Head;

		while (node1->key != _key1) node1 = node1->Next;
		while (node2->key != _key2) node2 = node2->Next;

		T tmp = node1->key;
		node1->key = node2->key;
		node2->key = tmp;
	}

	void DeleteList() {
		Double_Linked_Node<T> *tmp1 = Head;
		Double_Linked_Node<T> *tmp2;

		while (tmp1 != NULL) {
			tmp2 = tmp1;
			tmp1 = tmp1->Next;
			delete tmp2;
		}

		Head = NULL;
	}

	void AddToEnd(T _key) {
		Double_Linked_Node<T> *tmp = Head;
		while (tmp->Next != NULL) tmp = tmp->Next;

		Double_Linked_Node<T> *node = new Double_Linked_Node<T>;
		node->key = _key;
		tmp->Next = node;
		node->Next = NULL;
		node->Prev = tmp;
	}

	void Sort() {

		Double_Linked_Node<T> *node1;
		Double_Linked_Node<T> *node2;

		for (int i = 0; i < GetCount(); i++)
		{
			node1 = Head;
			node2 = node1->Next;
			while (node2 != NULL)
			{
				if (node1->key > node2->key)
				{
					Swap(node1->key, node2->key);
				}
				node1 = node1->Next;
				node2 = node2->Next;
			}
		}

	}

	void Merge(Double_Linked_List & B) {

		Double_Linked_Node<T> *tmp = B.Head;

		while (tmp != NULL)
		{
			AddToEnd(tmp->key);
			tmp = tmp->Next;
		}
	}
};